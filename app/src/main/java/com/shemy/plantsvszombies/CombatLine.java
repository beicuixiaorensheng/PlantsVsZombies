package com.shemy.plantsvszombies;

import android.util.SparseArray;

import org.cocos2d.actions.CCScheduler;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Dzsom on 2018/11/19.
 */

public class CombatLine {

    private SparseArray<Plant> plants;
    private ArrayList<Zombie> zombies;
    private ArrayList<ShootPlant> shootPlants;
    private ArrayList<ZombieImp> zombieImps;
    private ArrayList<Sun> suns;
    private ArrayList<PotatoMine> potatoMines;
    private ZombieImp zombieImpa;

    public CombatLine() {
        plants=new SparseArray<>();
        zombies=new ArrayList<>();
        zombieImps=new ArrayList<>();
        suns=new ArrayList<>();
        potatoMines=new ArrayList<>();
        shootPlants=new ArrayList<>();
        CCScheduler.sharedScheduler().schedule("attackPlant",this,1,false);
        CCScheduler.sharedScheduler().schedule("attackZombie",this,1,false);
        CCScheduler.sharedScheduler().schedule("bulletHurtCompute",this,0.2f,false);
        CCScheduler.sharedScheduler().schedule("potatoBomb",this,0.2f,false);
    }

    public void addPlant(int col,Plant plant){
        plants.put(col,plant);
        if (plant instanceof ShootPlant){
            shootPlants.add((ShootPlant)plant);
        }
        if (plant instanceof PotatoMine){
            potatoMines.add((PotatoMine) plant);
        }
    }

    public void removePlant(int col){
        plants.get(col).removeSelf();
        plants.delete(col);
    }

    public boolean isContainPlant(int col){
        if (plants.get(col)!=null){
            return true;
        }

        return false;
    }

    public void addZombie(Zombie zombie){
        zombies.add(zombie);
    }

    public void addZombieImps(ZombieImp zombieImp){
        zombieImps.add(zombieImp);
    }

    public void  addSun(Sun sun){
        suns.add(sun);
    }

    public void attackPlant(float t){
        if (zombies.size()!=0&&plants.size()!=0){
            for (Zombie zombie :zombies){
                int col=(int)(zombie.getPosition().x-280)/105;
                if (isContainPlant(col)){
                    switch (zombie.getState()){
                        case MOVE:
                            zombie.stopAllActions();
                            zombie.attaack();
                            break;

                        case ATTACK:
                            Plant plant = plants.get(col);
                            plant.hurtCompute(zombie.getAttack());
                            if (plant.getHP()==0){
                                plants.remove(col);
                                plant.removeSelf();
                                zombie.stopAllActions();
                                zombie.move();
                            }
                            break;
                    }
                }else if (zombie.getState()==Zombie.State.ATTACK){
                    zombie.stopAllActions();
                    zombie.move();
                }
            }
        }

        if (zombieImps.size()!=0&&plants.size()!=0){
            for (ZombieImp zombieImp :zombieImps){
                int col=(int)(zombieImp.getPosition().x-280)/105;
                if (isContainPlant(col)){
                    switch (zombieImp.getState()){
                        case MOVE:
                            zombieImp.stopAllActions();
                            zombieImp.attaack();
                            break;

                        case ATTACK:
                            Plant plant = plants.get(col);
                            plant.hurtCompute(zombieImp.getAttack());
                            if (plant.getHP()==0){
                                plants.remove(col);
                                plant.removeSelf();
                                zombieImp.stopAllActions();
                                zombieImp.move();
                            }
                            break;
                    }
                }else if (zombieImp.getState()==ZombieImp.State.ATTACK){
                    zombieImp.stopAllActions();
                    zombieImp.move();
                }
            }
        }
    }

    public void attackZombie(float t){
        if (!shootPlants.isEmpty()){
            for (ShootPlant shootPlant:shootPlants){
                if (zombies.isEmpty() && zombieImps.isEmpty()){
                    shootPlant.stopAttackZombie();
                }else {
                    shootPlant.attackZombie();
                }
            }
        }
    }

    public void potatoBomb(float t){
         if (!potatoMines.isEmpty() && !zombies.isEmpty()){
             for (PotatoMine potatoMine:potatoMines){
                 if (potatoMine.getUp()) {
                     Iterator<Zombie> iterator = zombies.iterator();
                     while (iterator.hasNext()) {
                         Zombie zombie = iterator.next();
                         if (potatoMine.getPosition().x > zombie.getPosition().x-20
                                 && potatoMine.getPosition().x < zombie.getPosition().x + 20) {
                             int col = (int) (zombie.getPosition().x - 280) / 105;
                             zombie.hurrtCompute(potatoMine.getAttack());
                             if (zombie.getHP() == 0) {
                                 zombie.stopAllActions();
                                 zombie.dieBomb();
                                 iterator.remove();
                                 potatoMine.boom();
                                 potatoMines.remove(potatoMine);
                                 plants.remove(col);
                             }
                         }
                     }
                 }
             }
         }

        if (!potatoMines.isEmpty() && !zombieImps.isEmpty()){
            for (PotatoMine potatoMine:potatoMines){
                if (potatoMine.getUp()) {
                    Iterator<ZombieImp> iterator = zombieImps.iterator();
                    while (iterator.hasNext()) {
                        ZombieImp zombieImp = iterator.next();
                        if (potatoMine.getPosition().x > zombieImp.getPosition().x-20
                                && potatoMine.getPosition().x < zombieImp.getPosition().x + 20) {
                            int col = (int) (zombieImp.getPosition().x - 280) / 105;
                            zombieImp.hurrtCompute(potatoMine.getAttack());
                            if (zombieImp.getHP() == 0) {
                                zombieImp.stopAllActions();
                                zombieImp.dieBomb();
                                iterator.remove();
                                potatoMine.boom();
                                potatoMines.remove(potatoMine);
                                plants.remove(col);
                            }
                        }
                    }
                }
            }
        }
    }

    public void bulletHurtCompute(float t){

        if (!shootPlants.isEmpty()&&!zombies.isEmpty()){
            for (ShootPlant shootPlant:shootPlants){
                for (Bullet bullet:shootPlant.getBullets()){
                    Iterator<Zombie> iterator = zombies.iterator();
                    while (iterator.hasNext()){
                        Zombie zombie = iterator.next();
                        if (bullet.getVisible()&&bullet.getPosition().x>zombie.getPosition().x-20
                                &&bullet.getPosition().x<zombie.getPosition().x+20){
                            bullet.showBulletBlast(zombie);
                            bullet.setVisible(false);
                            zombie.hurrtCompute(bullet.getAttack());
                            if (zombie.getHP()==0){
                                zombie.removeSelf();
                                iterator.remove();
                            }
                        }
                    }
                }
            }
        }

        if (!shootPlants.isEmpty()&&!zombieImps.isEmpty()){
            for (ShootPlant shootPlant:shootPlants){
                for (Bullet bullet:shootPlant.getBullets()){
                    Iterator<ZombieImp> iterator = zombieImps.iterator();
                    while (iterator.hasNext()){
                        ZombieImp zombieImp = iterator.next();
                        if (bullet.getVisible()&&bullet.getPosition().x>zombieImp.getPosition().x-80
                                &&bullet.getPosition().x<zombieImp.getPosition().x+20){
                            bullet.showBulletBlast(zombieImp);
                            bullet.setVisible(false);
                            zombieImp.hurrtCompute(bullet.getAttack());
                            if (zombieImp.getHP()==0){
                                zombieImp.stopAllActions();
                                zombieImp.die(zombieImp);
                                zombieImpa=zombieImp;
                                CCCallFunc ccCallFunc = CCCallFunc.action(this,"removeZombieImp");
                                CCDelayTime ccDelayTime = CCDelayTime.action(2);
                                CCSequence ccSequence = CCSequence.actions(ccDelayTime,ccCallFunc);
                                zombieImp.runAction(ccSequence);
                                iterator.remove();
                            }
                        }
                    }
                }
            }
        }
    }

    public void removeZombieImp(){
        zombieImpa.removeSelf();
    }
}
