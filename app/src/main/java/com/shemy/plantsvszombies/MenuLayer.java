package com.shemy.plantsvszombies;

import android.view.MotionEvent;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeBLTransition;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.ccColor3B;

/**
 * Created by Dzsom on 2018/11/6.
 */

class MenuLayer extends CCLayer {

    private CCSprite ccSprite_dialog;
    private CCSprite ccSprite_BackGame;
    private CCSprite ccSprite_BackMenu;
    private boolean isShow=false;

    public MenuLayer() {
        CCSprite ccSprite_menu = CCSprite.sprite("menu/main_menu_bg.png");
        ccSprite_menu.setAnchorPoint(0,0);
        addChild(ccSprite_menu);

        CCMenu ccMenu = CCMenu.menu();
        CCSprite ccSprite_start_adventure_default=
                CCSprite.sprite("menu/start_adventure_default.png");
        CCSprite ccSprite_start_adventure_press =
                CCSprite.sprite("menu/start_adventure_press.png");
        CCMenuItemSprite ccMenuItemSprite = CCMenuItemSprite.item(ccSprite_start_adventure_default,
                ccSprite_start_adventure_press,this,"start");
        ccMenuItemSprite.setPosition(270,160);
        ccMenu.addChild(ccMenuItemSprite);
        addChild(ccMenu);
        setIsTouchEnabled(true);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        CGPoint cgPoint = convertTouchToNodeSpace(event);
        System.out.println("cgPoint"+cgPoint);
        CGRect helpRect=CGRect.make(1050,50,100,130);
        if(CGRect.containsPoint(helpRect,cgPoint)){
            CCScene ccScene = CCScene.node();
            ccScene.addChild(new HelpLayer());
            CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,ccScene);
            CCDirector.sharedDirector().runWithScene(ccFadeTransition);
        }
        CGRect optionRect=CGRect.make(920,90,130,100);
        if(CGRect.containsPoint(optionRect,cgPoint)){
            System.out.println("Option");
        }
        CGRect outRect=CGRect.make(1150,60,100,130);

        if (isShow){
            //返回游戏
            if(CGRect.containsPoint(ccSprite_BackGame.getBoundingBox(),cgPoint)){
                ccSprite_dialog.removeSelf();
                ccSprite_BackMenu.removeSelf();
                ccSprite_BackGame.removeSelf();
                isShow=false;
            }
            if (CGRect.containsPoint(ccSprite_BackMenu.getBoundingBox(),cgPoint)){
                System.exit(0);
            }
        }else {
            //退出弹窗
            if (CGRect.containsPoint(outRect, cgPoint)) {
                isShow = true;
                dialog();
            }
        }
        return super.ccTouchesBegan(event);
    }

    //弹窗
    public void dialog(){
        ccSprite_dialog=CCSprite.sprite("dialog/dialog.png");
        ccSprite_dialog.setPosition(650,400);
        ccSprite_dialog.setScale(2.0);
        addChild(ccSprite_dialog);

        CCLabel ccLabel=CCLabel.makeLabel("提示","",36);
        ccLabel.setColor(ccColor3B.ccWHITE);
        ccLabel.setPosition(250,250);
        ccSprite_dialog.addChild(ccLabel);

        CCLabel ccLabel_music=CCLabel.makeLabel("确定退出游戏?","",28);
        ccLabel_music.setColor(ccColor3B.ccGREEN);
        ccLabel_music.setPosition(250,200);
        ccSprite_dialog.addChild(ccLabel_music);
        ccSprite_BackGame=CCSprite.sprite("dialog/back_game.png");
        ccSprite_BackGame.setScale(2.5);
        ccSprite_BackGame.setPosition(440,120);
        addChild(ccSprite_BackGame);
        ccSprite_BackMenu=CCSprite.sprite("dialog/exit.png");
        ccSprite_BackMenu.setScale(2.5);
        ccSprite_BackMenu.setPosition(880,120);
        addChild(ccSprite_BackMenu);


    }

    public void start(Object item){
        CCScene ccScene = CCScene.node();
        ccScene.addChild(new CombatLayer());
        CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,ccScene);
        CCDirector.sharedDirector().runWithScene(ccFadeTransition);
    }
}
