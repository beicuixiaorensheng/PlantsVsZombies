package com.shemy.plantsvszombies;

/**
 * Created by Dzsom on 2018/11/14.
 */

public class SnowPea extends ShootPlant {
    public SnowPea() {
        super("plant/SnowPea/Frame%02d.png",15);
        setPrice(175);
    }

    @Override
    public void createBullet(float t) {

        SnowBullet snowBullet = new SnowBullet(this);
    }
}
