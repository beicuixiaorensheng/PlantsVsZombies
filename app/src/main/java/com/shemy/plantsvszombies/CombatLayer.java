package com.shemy.plantsvszombies;

import android.view.MotionEvent;

import org.cocos2d.actions.CCScheduler;
import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCBlink;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.layers.CCTMXObjectGroup;
import org.cocos2d.layers.CCTMXTiledMap;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.transitions.CCFlipXTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;
import org.cocos2d.types.util.CGPointUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Dzsom on 2018/11/8.
 */

class CombatLayer extends CCLayer {

    private CGSize winSize;
    private CCSprite ccSprite_SeedBank;
    private CCLabel ccLabel;
    private CCSprite ccSprite_SeedChooser;
    private ArrayList<PlantCard>  plantCards;
    private ArrayList<PlantCard>  selectPlantCards;
    private boolean isMove;
    private CCSprite ccSprite_SeedChooser_button;
    private CCTMXTiledMap cctmxTiledMap;
    private ArrayList<CCSprite> ccSprites_show;
    private CCSprite ccSprite_startReady;
    private boolean isStart;
    private PlantCard selectCard;
    private Plant selectPlant;
    private ArrayList<ArrayList<CGPoint>> cgPoints_towers;
    private ArrayList<CombatLine> combatLines;
    private ArrayList<CGPoint> cgPoints_path;
    private Random random;
    private int currentSunNumber=50;
    private ArrayList<Sun> suns;
    private Sun sun;
    private CCSprite ccSprite_menuButton;
    private CCSprite ccSprite_ShovelBack;
    private CCSprite ccSprite_Shovel;
    private Boolean isShovel=false;
    private CCSprite ccSprite_BackGame;
    private CCSprite ccSprite_BackMenu;
    private boolean isShow=false;
    private CCSprite ccSprite_dialog;
    private boolean isMusic=true;
    private CCLabel ccLabel1_isMusic;
    private CCLabel ccLabel1_notMusic;
    private int type;

    public CombatLayer() {
        playBackgroundMusic(R.raw.lookup,true);
       loadMap();
    }

    private void loadMap() {
        cctmxTiledMap = CCTMXTiledMap.tiledMap("combat/map1.tmx");
        addChild(cctmxTiledMap);
        CCTMXObjectGroup objectGroup_show = cctmxTiledMap.objectGroupNamed("show");
        ArrayList<HashMap<String,String>> objects = objectGroup_show.objects;
        ccSprites_show=new ArrayList<>();
        for (HashMap<String,String> object:objects){
            float x =Float.parseFloat(object.get("x"));
            float y =Float.parseFloat(object.get("y"));
            CCSprite ccSprite_shake;
            if (type<9) {
                ccSprite_shake = CCSprite.sprite("zombies/zombies_1/shake/Frame00.png");
                ccSprite_shake.setPosition(x, y);
                cctmxTiledMap.addChild(ccSprite_shake);
                ccSprites_show.add(ccSprite_shake);
                ArrayList<CCSpriteFrame> frames = new ArrayList<>();
                for (int i = 0; i < 2; i++) {
                    CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,
                            "zombies/zombies_1/shake/Frame%02d.png", i)).displayedFrame();
                    frames.add(ccSpriteFrame);
                }
                CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames, 0.2f);
                CCAnimate ccAnimate = CCAnimate.action(ccAnimation, true);
                CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccAnimate);
                ccSprite_shake.runAction(ccRepeatForever);
                type++;
            }else {
                ccSprite_shake=CCSprite.sprite("zombieswonimp/Frame00.png");
                ccSprite_shake.setPosition(x,y);
                cctmxTiledMap.addChild(ccSprite_shake);
                ccSprites_show.add(ccSprite_shake);
                ArrayList<CCSpriteFrame> frames=new ArrayList<>();
                for (int i = 0; i < 2; i++) {
                    CCSpriteFrame ccSpriteFrame=CCSprite.sprite(String.format(Locale.CHINA,
                            "zombieswonimp/Frame%02d.png", i)).displayedFrame();
                    frames.add(ccSpriteFrame);
                }
                CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames, 0.2f);
                CCAnimate ccAnimate = CCAnimate.action(ccAnimation, true);
                CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccAnimate);
                ccSprite_shake.runAction(ccRepeatForever);
            }
        }
        winSize= CCDirector.sharedDirector().winSize();
        CCDelayTime ccDelayTime = CCDelayTime.action(2);
        CCMoveBy ccMoveBy = CCMoveBy.action(2,ccp(winSize.getWidth()-cctmxTiledMap.
                getContentSize().getWidth(),0));
        CCCallFunc ccCallFunc =CCCallFunc.action(this,"loadChoose");
        CCSequence ccSequence =CCSequence.actions(ccDelayTime,ccMoveBy,ccCallFunc);
        cctmxTiledMap.runAction(ccSequence);
    }

    public void loadChoose(){
        ccSprite_SeedBank = CCSprite.sprite("choose/SeedBank.png");
        ccSprite_SeedBank.setAnchorPoint(0,1);
        ccSprite_SeedBank.setPosition(0,winSize.getHeight());
        addChild(ccSprite_SeedBank);

        ccLabel=CCLabel.makeLabel("50","",20);
        ccLabel.setColor(ccColor3B.ccBLACK);
        ccLabel.setPosition(40,695);
        addChild(ccLabel);

        ccSprite_SeedChooser= CCSprite.sprite("choose/SeedChooser.png");
        ccSprite_SeedChooser.setAnchorPoint(0,0);
        addChild(ccSprite_SeedChooser);

        CCSprite ccSprite_SeedChooser_Button_Disabled=CCSprite.
                sprite("choose/SeedChooser_Button_Disabled.png");
        ccSprite_SeedChooser_Button_Disabled
                .setPosition(ccSprite_SeedChooser.getContentSize().getWidth()/2,80);
        ccSprite_SeedChooser.addChild(ccSprite_SeedChooser_Button_Disabled);

        ccSprite_SeedChooser_button = CCSprite.sprite("choose/SeedChooser_Button.png");
        ccSprite_SeedChooser_button.
                setPosition(ccSprite_SeedChooser.getContentSize().getWidth()/2,80);
        ccSprite_SeedChooser.addChild(ccSprite_SeedChooser_button);
        ccSprite_SeedChooser_button.setVisible(false);

        plantCards= new ArrayList<>();
        selectPlantCards=new ArrayList<>();
        for (int i = 0; i <8 ; i++) {
            PlantCard plantCard = new PlantCard(i);
            plantCards.add(plantCard);
            plantCard.getDark().setPosition(50+60*i,590);
            ccSprite_SeedChooser.addChild(plantCard.getDark());
            plantCard.getLight().setPosition(50+60*i,590);
            ccSprite_SeedChooser.addChild(plantCard.getLight());
        }
        setIsTouchEnabled(true);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        CGPoint cgPoint = convertTouchToNodeSpace(event);
        System.out.println("cgPoint"+cgPoint);
        if (isStart){
            if (CGRect.containsPoint(ccSprite_SeedBank.getBoundingBox(),cgPoint)) {
                isShovel=false;
                ccSprite_Shovel.stopAllActions();
                ccSprite_Shovel.setVisible(true);
                if (selectCard!=null){
                    if (currentSunNumber >= selectPlant.getPrice()){
                        selectCard.getLight().setOpacity(255);
                        selectCard=null;
                    }else {
                        selectCard.getLight().setOpacity(30);
                        selectCard=null;
                    }
                }
                for (PlantCard plantCard:selectPlantCards){
                    if (CGRect.containsPoint(plantCard.getLight().getBoundingBox(),cgPoint)) {
                        selectCard=plantCard;
                        selectCard.getLight().setOpacity(100);
                        /*CCBlink ccBlink=CCBlink.action(2,2);
                        CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccBlink);
                        selectCard.getLight().runAction(ccRepeatForever);*/
                        switch (selectCard.getId()){
                            case 0:
                                selectPlant=new Peashooter();
                                break;
                            case 1:
                                selectPlant=new SunFlower();
                                break;
                            case 2:
                                selectPlant=new CherryBomb();
                                break;
                            case 3:
                                selectPlant=new WallNut();
                                break;
                            case 4:
                                selectPlant=new PotatoMine();
                                break;
                            case 5:
                                selectPlant=new SnowPea();
                                break;
                            case 6:
                                selectPlant=new Chomper();
                                break;
                            case 7:
                                selectPlant=new Repeater();
                                break;
                        }
                    }
                }
            }else if (selectPlant!=null&&selectCard!=null) {
                if (currentSunNumber >= selectPlant.getPrice()) {
                    int col = (int) (cgPoint.x - 220) / 105;
                    int row = (int) (cgPoint.y - 40) / 120;
                    if (col >= 0 && col < 9 && row >= 0 && row < 5) {
                        CombatLine combatLine = combatLines.get(row);
                        if (!combatLine.isContainPlant(col)) {
                            combatLine.addPlant(col, selectPlant);
                            selectPlant.setPosition(cgPoints_towers.get(row).get(col));
                            if (selectPlant==new SunFlower()){
                                cctmxTiledMap.addChild(selectPlant,3);
                            }else {
                                cctmxTiledMap.addChild(selectPlant,1);
                            }
                            addSunNumber(-selectPlant.getPrice());
                            selectPlant = null;
                            selectCard = null;
                        }
                    }
                }
            }

            if (isShow){
                if (CGRect.containsPoint(ccLabel1_isMusic.getBoundingBox(),cgPoint)){
                    if (isMusic){
                        ccLabel1_isMusic.setVisible(false);
                        ccLabel1_notMusic.setVisible(true);
                        isMusic=false;
                    }else {
                        ccLabel1_isMusic.setVisible(true);
                        ccLabel1_notMusic.setVisible(false);
                        isMusic=true;
                    }

                }

                //返回游戏
                if(CGRect.containsPoint(ccSprite_BackGame.getBoundingBox(),cgPoint)){
                    CCDirector.sharedDirector().resume();
                    SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.pause,false);
                    ccSprite_dialog.removeSelf();
                    ccSprite_BackMenu.removeSelf();
                    ccSprite_BackGame.removeSelf();
                    ccLabel1_isMusic.removeSelf();
                    ccLabel1_notMusic.removeSelf();
                    ccSprite_menuButton.setVisible(true);
                    if (isMusic){
                        playBackgroundMusic(R.raw.lookup,true);
                    }
                    isShow=false;
                }
                if (CGRect.containsPoint(ccSprite_BackMenu.getBoundingBox(),cgPoint)){
                    CCDirector.sharedDirector().resume();
                    SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.pause,false);
                    isShow=false;
                    restart();
                }
            }else {
                //点击菜单按钮弹出菜单框
                if (CGRect.containsPoint(ccSprite_menuButton.getBoundingBox(), cgPoint)) {
                    SoundEngine.sharedEngine().playSound
                            (CCDirector.theApp, R.raw.pause, false);
                    isShow = true;
                    ccSprite_menuButton.setVisible(false);
                    CCDirector.sharedDirector().pause();
                    dialog();
                }
            }

            //铲子
            if(CGRect.containsPoint(ccSprite_ShovelBack.getBoundingBox(),cgPoint)){
                if (!isShovel){
                    selectPlant = null;
                    selectCard = null;
                    isShovel=true;
                    CCBlink ccBlink=CCBlink.action(2,5);
                    CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccBlink);
                    ccSprite_Shovel.runAction(ccRepeatForever);
                }else {
                    isShovel=false;
                    ccSprite_Shovel.stopAllActions();
                    ccSprite_Shovel.setVisible(true);
                }
            }else if (isShovel){
                int col = (int) (cgPoint.x - 220) / 105;
                int row = (int) (cgPoint.y - 40) / 120;
                if (col >= 0 && col < 9 && row >= 0 && row < 5) {
                    CombatLine combatLine = combatLines.get(row);
                    if (combatLine.isContainPlant(col)) {
                        combatLine.removePlant(col);
                        isShovel=false;
                        ccSprite_Shovel.stopAllActions();
                        ccSprite_Shovel.setVisible(true);
                    }
                }
            }


            //阳光
            if(suns!=null){
                for (Sun sun : suns) {
                    if (CGRect.containsPoint(sun.getBoundingBox(), cgPoint)) {
                        isShovel=false;
                        ccSprite_Shovel.stopAllActions();
                        ccSprite_Shovel.setVisible(true);
                        sun.collect();
                    }
                }
            }
        }else {
            if (CGRect.containsPoint(ccSprite_SeedChooser.getBoundingBox(), cgPoint)) {
                if (selectPlantCards.size() < 6) {
                    for (PlantCard plantCard : plantCards) {
                        if (CGRect.containsPoint(plantCard.getLight().getBoundingBox(), cgPoint)) {
                            if (!selectPlantCards.contains(plantCard)) {
                                selectPlantCards.add(plantCard);
                                CCMoveTo ccMoveTo = CCMoveTo.action(0.1f,
                                        ccp(50 + 60 * selectPlantCards.size(), 725));
                                plantCard.getLight().runAction(ccMoveTo);
                                if (selectPlantCards.size() == 6) {
                                    ccSprite_SeedChooser_button.setVisible(true);
                                }
                            }
                        }
                    }
                }
            }
            if (CGRect.containsPoint(ccSprite_SeedBank.getBoundingBox(), cgPoint)) {
                isMove = false;
                for (PlantCard plantCard : selectPlantCards) {
                    if (CGRect.containsPoint(plantCard.getLight().getBoundingBox(), cgPoint)) {
                        CCMoveTo ccMoveTo = CCMoveTo.action(0.1f,
                                plantCard.getDark().getPosition());
                        plantCard.getLight().runAction(ccMoveTo);
                        selectPlantCards.remove(plantCard);
                        ccSprite_SeedChooser_button.setVisible(false);
                        isMove = true;
                        break;
                    }
                }
            }
            if (isMove) {
                for (int i = 0; i < selectPlantCards.size(); i++) {
                    PlantCard plantCard = selectPlantCards.get(i);
                    CCMoveTo ccMoveTo = CCMoveTo.action(0.1f, ccp(110 + 60 * i, 725));
                    plantCard.getLight().runAction(ccMoveTo);
                }
            }
            if (ccSprite_SeedChooser_button.getVisible()) {
                if (CGRect.containsPoint(ccSprite_SeedChooser_button.getBoundingBox(), cgPoint)) {
                    for (PlantCard plantCard : selectPlantCards) {
                        addChild(plantCard.getLight());
                    }
                    setIsTouchEnabled(false);
                    ccSprite_SeedChooser.removeSelf();
                    CCMoveTo ccMoveTo = CCMoveTo.action(2, ccp(-100, 0));
                    CCCallFunc ccCallFunc = CCCallFunc.action(this, "startReady");
                    CCSequence ccSequence = CCSequence.actions(ccMoveTo, ccCallFunc);
                    cctmxTiledMap.runAction(ccSequence);
                }
            }
        }
        return super.ccTouchesBegan(event);
    }

    //弹窗
    public void dialog(){
        ccSprite_dialog=CCSprite.sprite("dialog/dialog.png");
        ccSprite_dialog.setPosition(650,400);
        ccSprite_dialog.setScale(2.0);
        addChild(ccSprite_dialog);

        CCLabel ccLabel=CCLabel.makeLabel("选项","",36);
        ccLabel.setColor(ccColor3B.ccWHITE);
        ccLabel.setPosition(250,250);
        ccSprite_dialog.addChild(ccLabel);

        CCLabel ccLabel_music=CCLabel.makeLabel("音乐:","",28);
        ccLabel_music.setColor(ccColor3B.ccGREEN);
        ccLabel_music.setPosition(200,200);
        ccSprite_dialog.addChild(ccLabel_music);
        ccLabel1_isMusic=CCLabel.makeLabel("开","",50);
        ccLabel1_isMusic.setColor(ccColor3B.ccGREEN);
        ccLabel1_isMusic.setPosition(700,430);
        ccLabel1_notMusic=CCLabel.makeLabel("关","",50);
        ccLabel1_notMusic.setColor(ccColor3B.ccRED);
        ccLabel1_notMusic.setPosition(700,430);
        if (isMusic){
            ccLabel1_isMusic.setVisible(true);
            ccLabel1_notMusic.setVisible(false);
        }else {
            ccLabel1_isMusic.setVisible(false);
            ccLabel1_notMusic.setVisible(true);
        }
        addChild(ccLabel1_isMusic);
        addChild(ccLabel1_notMusic);
        ccSprite_BackGame=CCSprite.sprite("dialog/back_game.png");
        ccSprite_BackGame.setScale(2.5);
        ccSprite_BackGame.setPosition(440,120);
        addChild(ccSprite_BackGame);
        ccSprite_BackMenu=CCSprite.sprite("dialog/back_menu.png");
        ccSprite_BackMenu.setScale(2.5);
        ccSprite_BackMenu.setPosition(880,120);
        addChild(ccSprite_BackMenu);


    }

    public void startReady(){
        for (CCSprite ccSprite : ccSprites_show){
            ccSprite.removeSelf();
        }
        setIsTouchEnabled(false);
        ccSprite_startReady= CCSprite.sprite("startready/startReady_00.png");
        ccSprite_startReady.setPosition(winSize.getWidth()/2,winSize.getHeight()/2);
        addChild(ccSprite_startReady);
        ArrayList<CCSpriteFrame> frames = new ArrayList<>();
        for (int i = 0; i <3 ; i++) {
            CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,
                    "startready/startReady_%02d.png",i)).displayedFrame();
            frames.add(ccSpriteFrame);
        }
        //10秒产生一个太阳
        CCScheduler.sharedScheduler().schedule("dropSun",this,10,false);

        //游戏中的菜单
        ccSprite_menuButton=CCSprite.sprite("menu/menu_button.png");
        ccSprite_menuButton.setScale(1.5);
        ccSprite_menuButton.setPosition(1180,730);
        addChild(ccSprite_menuButton);

        //铲子背景
        ccSprite_ShovelBack=CCSprite.sprite("shovel/ShovelBack.png");
        ccSprite_ShovelBack.setScale(2.0);
        ccSprite_ShovelBack.setPosition(520,730);
        addChild(ccSprite_ShovelBack);

        //铲子
        ccSprite_Shovel=CCSprite.sprite("shovel/Shovel.png");
        ccSprite_Shovel.setScale(1.0);
        ccSprite_Shovel.setPosition(520,730);
        addChild(ccSprite_Shovel);

        CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames,0.2f);
        CCAnimate ccAnimate = CCAnimate.action(ccAnimation,false);
        CCCallFunc ccCallFunc= CCCallFunc.action(this,"start");
        CCSequence ccSequence = CCSequence.actions(ccAnimate,ccCallFunc);
        ccSprite_startReady.runAction(ccSequence);
    }

    public void menu(){
        CCScene ccScene=CCScene.node();
        ccScene.addChild(new MenuLayer());
        CCFadeTransition ccFadeTransition=CCFadeTransition.transition(2,ccScene);
        CCDirector.sharedDirector().runWithScene(ccFadeTransition);
    }

    public void start(){
        ccSprite_startReady.removeSelf();
        setIsTouchEnabled(true);
        isStart=true;
        SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.faster,true);
        cgPoints_towers=new ArrayList<>();
        for (int i = 0; i <5 ; i++) {
            ArrayList<CGPoint> cgPoints_tower=  new ArrayList<>();
            CCTMXObjectGroup objectGroup_tower= cctmxTiledMap.objectGroupNamed("tower"+i);
            ArrayList<HashMap<String,String>> objects = objectGroup_tower.objects;
            for (HashMap<String,String> object: objects){
                float x = Float.parseFloat(object.get("x"));
                float y = Float.parseFloat(object.get("y"));
                cgPoints_tower.add(ccp(x,y));
            }
            cgPoints_towers.add(cgPoints_tower);
        }
        combatLines = new ArrayList<>();
        for (int i = 0; i <5 ; i++) {
            combatLines.add(new CombatLine());
        }

        cgPoints_path=new ArrayList<>();
        CCTMXObjectGroup objectGroup_path=cctmxTiledMap.objectGroupNamed("path");
        ArrayList<HashMap<String,String>> objects = objectGroup_path.objects;
        for (HashMap<String,String> object: objects){
            float x = Float.parseFloat(object.get("x"));
            float y = Float.parseFloat(object.get("y"));
            cgPoints_path.add(ccp(x,y));
        }
        random=new Random();
        suns=new ArrayList<>();
        update();
        CCDelayTime ccDelayTime1=CCDelayTime.action(20);
        CCCallFunc ccCallFunc1=CCCallFunc.action(this,"startAddZombie1");
        CCDelayTime ccDelayTime2=CCDelayTime.action(40);
        CCCallFunc ccCallFunc2=CCCallFunc.action(this,"startAddZombie2");
        CCDelayTime ccDelayTime3=CCDelayTime.action(60);
        CCCallFunc ccCallFunc3=CCCallFunc.action(this,"startAddZombie3");
        CCSequence ccSequence=CCSequence.actions(ccDelayTime1,ccCallFunc1,ccDelayTime2,ccCallFunc2,
                ccDelayTime3,ccCallFunc3);
        runAction(ccSequence);
    }

    public void startAddZombie1(){
        zombieSound();
        CCScheduler.sharedScheduler().schedule("addZombieImp",this,
                20,false);
    }
    public void startAddZombie2(){
        zombieSound();
        CCScheduler.sharedScheduler().schedule("addZombieImp",this,
                10,false);
    }
    public void startAddZombie3(){
        zombieSound();
        CCScheduler.sharedScheduler().schedule("addZombie",this,
                5,false);
    }

    public void addZombie(float t){
        int i = random.nextInt(5);
        Zombie zombie = new Zombie(this,cgPoints_path.get(2*i),
                cgPoints_path.get(2*i+1));
        cctmxTiledMap.addChild(zombie,5-i);
        combatLines.get(i).addZombie(zombie);
    }

    public void addZombieImp(float t){
        int i = random.nextInt(5);
        ZombieImp zombieImp = new ZombieImp(this,cgPoints_path.get(2*i),
                cgPoints_path.get(2*i+1));
        cctmxTiledMap.addChild(zombieImp,5-i);
        combatLines.get(i).addZombieImps(zombieImp);
    }

    public void zombieSound(){
        if(isMusic){
            SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.awooga,false);
        }else {
            SoundEngine.sharedEngine().pauseSound();
        }


    }

    //随机太阳掉落
    public void  dropSun(float t){
        int i=random.nextInt(750);
        int j=random.nextInt(520);
        sun =new Sun();
        CGPoint start = ccp(250 + i, 768);
        CGPoint end =ccp(250+i,80+j);
        sun.setPosition(start);
        addChild(sun);
        suns.add(sun);
        float v = CGPointUtil.distance(start, end) / 150;
        CCMoveTo ccMoveTo=CCMoveTo.action(v,end);
        CCDelayTime ccDelayTime = CCDelayTime.action(5);
        CCCallFunc ccCallFunc = CCCallFunc.action(this, "removeSun");
        CCSequence ccSequence = CCSequence.actions(ccMoveTo, ccDelayTime, ccCallFunc);
        sun.runAction(ccSequence);

    }

    public void removeSun(){
        removeSun(sun);
        if (!sun.isNowCollect()){
            sun.removeSelf();
        }
    }

    public void end(){
        setIsTouchEnabled(false);
        CCScheduler.sharedScheduler().unschedule("addZombie",this);
        CCScheduler.sharedScheduler().unschedule("addZombieImp",this);
        CCScheduler.sharedScheduler().unschedule("dropSun",this);
        for (CCNode ccNode:cctmxTiledMap.getChildren()){
            ccNode.stopAllActions();
            ccNode.unscheduleAllSelectors();
        }
        for (CCNode ccNode:getChildren()){
            ccNode.stopAllActions();
            ccNode.unscheduleAllSelectors();
        }
        if (isMusic){
            SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.scream,false);
        }
        CCSprite ccSprite_ZombieWon= CCSprite.sprite("zombieswon/ZombiesWon.png");
        ccSprite_ZombieWon.setPosition(winSize.getWidth()/2,winSize.getHeight()/2);
        addChild(ccSprite_ZombieWon);
        CCDelayTime ccDelayTime = CCDelayTime.action(2);
        CCCallFunc ccCallFunc = CCCallFunc.action(this,"restart");
        CCSequence ccSequence = CCSequence.actions(ccDelayTime,ccCallFunc);
        ccSprite_ZombieWon.runAction(ccSequence);
    }

    public void restart(){
        CCScene ccScene = CCScene.node();
        ccScene.addChild(new MenuLayer());
        CCFlipXTransition ccFlipTransition = CCFlipXTransition.
                transition(2,ccScene,1);
        SoundEngine.sharedEngine().playSound(CCDirector.theApp,R.raw.mountains,true);
        CCDirector.sharedDirector().replaceScene(ccFlipTransition);
    }

    public void addSunNumber(int number) {
        currentSunNumber+=number;
        ccLabel.setString(currentSunNumber+"");
        update();
    }

    private void update() {
        for (PlantCard plantCard:selectPlantCards){
            int price=0;
            switch (plantCard.getId()){
                case 0:
                    price=100;
                    break;
                case 1:
                    price=50;
                    break;
                case 2:
                    price=150;
                    break;
                case 3:
                    price=50;
                    break;
                case 4:
                    price=25;
                    break;
                case 5:
                    price=175;
                    break;
                case 6:
                    price=150;
                    break;
                case 7:
                    price=200;
                    break;
            }
            if (currentSunNumber>=price){
                plantCard.getLight().setOpacity(255);
            }else {
                plantCard.getLight().setOpacity(30);
            }
        }
    }

    public void addSun(Sun sun) {
        suns.add(sun);
    }

    public void removeSun(Sun sun) {
        suns.remove(sun);
    }
}
