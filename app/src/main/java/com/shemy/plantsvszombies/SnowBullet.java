package com.shemy.plantsvszombies;

/**
 * Created by Dzsom on 2018/11/27.
 */

public class SnowBullet extends Bullet {
    public SnowBullet(ShootPlant shootPlant) {
        super( "bullet/bullet2.png",shootPlant);
        setAttack(10);
    }


    @Override
    public void showBulletBlast(Zombie zombie) {

        zombie.slow();
    }

    @Override
    public void showBulletBlast(ZombieImp zombieImp) {
        zombieImp.slow();
    }
}
